package com.playmusic;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyService extends Service {
    MediaPlayer mediaPlayer;
    public static final String CHANNEL_ID = "ForegroundService";
    int songPosition, media_length;
    String is = "";
    List<Song> list = new ArrayList<>();
    BroadcastReceiver receiver, actionReceiver;
    Bundle bundle;
    Intent intent1;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        bundle = intent.getExtras();
        intent1 = new Intent();
        intent1.setAction("dataFromService");
        if (bundle != null) {
            list = new ArrayList<>();
            list = (List<Song>) bundle.get("listSong");
            songPosition = (int) bundle.get("position");
            if (mediaPlayer != null) {
                mediaPlayer.stop();
            }
            receiver = new Receiver();

            initMediaPlayer(list.get(songPosition).getData());
            IntentFilter filter = new IntentFilter("positionData");
            registerReceiver(receiver, filter);
        }
        return START_NOT_STICKY;
    }


    private void sendNotification() {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_misic_note);
        MediaSessionCompat mediaSessionCompat = new MediaSessionCompat(this, "session");
        Song song = list.get(songPosition);
        Intent notificationIntent = new Intent(this, ListMusicActivity.class);
        notificationIntent.putExtra("position", songPosition + "");
        notificationIntent.putExtra("is", is);

        PendingIntent pendingIntent = PendingIntent.getActivity
                (this, 0, notificationIntent, PendingIntent.FLAG_MUTABLE);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_misic_note)
                .setContentTitle(song.title)
                .setContentText(song.getArtist())
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setLargeIcon(bitmap)
                .setOngoing(true)
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        .setShowActionsInCompactView(1, 2)
                        .setMediaSession(mediaSessionCompat.getSessionToken()));

        Intent previousIntent = new Intent(this, ActionBroadcastReceiver.class);
        previousIntent.setAction("Previous");
        PendingIntent pendingIntentPrevious = PendingIntent.getBroadcast(this, 12345, previousIntent, PendingIntent.FLAG_MUTABLE);
        builder.addAction(R.drawable.ic_previous, "Previous", pendingIntentPrevious);


        Intent pauseIntent = new Intent(this, ActionBroadcastReceiver.class);
        pauseIntent.setAction("Pause");
        PendingIntent pendingIntentPause = PendingIntent.getBroadcast(this, 12345, pauseIntent, PendingIntent.FLAG_MUTABLE);
        if (!is.equals("pause")) {
            builder.addAction(R.drawable.ic_pause, "Pause", pendingIntentPause);
        } else builder.addAction(R.drawable.ic_play, "Pause", pendingIntentPause);

        Intent nextIntent = new Intent(this, ActionBroadcastReceiver.class);
        nextIntent.setAction("Next");
        PendingIntent pendingIntentNext = PendingIntent.getBroadcast(this, 12345, nextIntent, PendingIntent.FLAG_MUTABLE);
        builder.addAction(R.drawable.ic_next, "Next", pendingIntentNext);


        Notification notification = builder.build();
        startForeground(1, notification);
    }

    private void initMediaPlayer(String pathAudio) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(pathAudio);
            mediaPlayer.setOnPreparedListener(onPreparedListener);
            mediaPlayer.setOnCompletionListener(onCompletionListener);
            mediaPlayer.prepare();
            mediaPlayer.start();
            sendNotification();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("positionData")) {
                if (intent.getBooleanExtra("FromNotification", false)) {
                    Bundle bundle = intent.getExtras();
                    if (bundle.getInt("action") == 0) {
                        songPosition--;
                        if (songPosition < 0) songPosition = list.size() - 1;
                        putPosition();
                        if (mediaPlayer != null) mediaPlayer.stop();
                        initMediaPlayer(list.get(songPosition).getData());

                    } else if (bundle.getInt("action") == 1) {
                        if (!is.equals("pause")) {
                            is = "pause";
                            if (mediaPlayer != null) mediaPlayer.pause();
                            media_length = mediaPlayer.getCurrentPosition();
                            Bundle bundle1 = new Bundle();
                            bundle1.putInt("current", media_length);
                            intent1.putExtras(bundle1);
                            putPosition();

                        } else {
                            is = "continue";
                            if (mediaPlayer != null) {
                                media_length = mediaPlayer.getCurrentPosition();
                                mediaPlayer.seekTo(media_length);
                                mediaPlayer.start();
                            }
                            putPosition();
                        }
                    } else if (bundle.getInt("action") == 2) {
                        songPosition++;
                        if (songPosition == list.size()) songPosition = 0;
                        putPosition();
                        if (mediaPlayer != null) mediaPlayer.stop();
                        initMediaPlayer(list.get(songPosition).getData());
                    }
                } else if (intent.getBooleanExtra("isGet", false)) {
                    if (mediaPlayer != null) {
                        Bundle bundle = intent.getExtras();
                        mediaPlayer.seekTo((Integer) bundle.get("currentPosition") * 1000);

                        mediaPlayer.start();
                        Log.d("aaaaaaaaa", "onReceive: " + bundle.get("currentPosition"));
                    }
                } else {
                    Log.d("TAGFFF", "onReceive2:");
                    Bundle bundle = intent.getExtras();
                    songPosition = (int) bundle.get("position");
                    is = (String) bundle.get("is");
                    switch (is) {
                        case "run":
                            mediaPlayer.stop();
                            initMediaPlayer(list.get(songPosition).getData());
                            break;
                        case "pause":
                            if (mediaPlayer != null) {
                                mediaPlayer.pause();
                            }
                            break;
                        case "continue":
                            if (mediaPlayer != null) {
                                media_length = mediaPlayer.getCurrentPosition();
                                mediaPlayer.seekTo(media_length);
                                mediaPlayer.start();
                            }
                            break;
                    }
                }
            }
        }
    }

    private void putPosition() {
        Bundle bundle = new Bundle();
        bundle.putInt("position", songPosition);
        bundle.putString("is", is);
        intent1.putExtras(bundle);
        sendBroadcast(intent1);
    }

    MediaPlayer.OnPreparedListener onPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {

        }
    };

    MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            songPosition++;
            if (songPosition == list.size()) songPosition = 0;
            putPosition();
            initMediaPlayer(list.get(songPosition).getData());
        }
    };


    @Override
    public void onDestroy() {
        if (mediaPlayer != null) mediaPlayer.stop();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
        if (actionReceiver != null) {
            unregisterReceiver(actionReceiver);
        }
        super.onDestroy();
    }
}

package com.playmusic;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlayMusicActivity extends AppCompatActivity {
    TextView songName, singerName;
    CircleImageView logo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_music);
        songName = findViewById(R.id.tv_sheet_song_name);
        singerName = findViewById(R.id.tv_singer_name);
        logo = findViewById(R.id.songLogo);
    }


}
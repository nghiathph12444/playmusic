package com.playmusic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListMusicActivity extends AppCompatActivity implements DataFromAdapter {
    List<Song> list;
    RecyclerView rcv;
    SongAdapter adapter;
    ConstraintLayout cLayout, ctlBottomSheet;
    TextView singerName, songName, tvSheetSongName, tvSheetSingerName, tvTime;
    ImageView imgPause, imgStop, imgNext, imgPrevious, imgDown, imgSheetNext, imgSheetPause, imgSheetPrevious;
    int songPosition, seekProgress = 0, delay = 1000;
    Bundle bundleService;
    boolean isPause;
    String is = "";
    BroadcastReceiver receiver;
    BottomSheetBehavior bottomSheetBehavior;
    SeekBar seekBar;
    Handler handler;
    Runnable myRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_music);

        isPause = true;

        anhXa();
        handler = new Handler();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 786);
        } else {
            getList();
        }
        clickNextOrPrevious();
        imgStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService();
            }
        });
        adapter = new SongAdapter(getList(), this, (DataFromAdapter) this);
        rcv.setAdapter(adapter);

        clickPause();
        myRunnable = new Runnable() {
            public void run() {
                if (!isPause) {
                    seekProgress += 1;
                    if (seekProgress == Integer.parseInt(list.get(songPosition).getDuration())) {
                        seekProgress = 0;
                    }
                }
                seekBar.setProgress(seekProgress);
                count();
            }
        };

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                songPosition = (int) bundle.get("position");
                handler.removeCallbacks(myRunnable);
                String status = (String) bundle.get("is");
                seekBar.setMax(getAllSecond());
                seekProgress = 0;
                if (status.equals("pause")) {
                    isPause = true;
                    imgPause.setImageResource(R.drawable.ic_play);
                    imgSheetPause.setImageResource(R.drawable.ic_play);
                    seekProgress = ((int) bundle.get("current")) / 1000;

                } else if (status.equals("contine")) {
                    isPause = false;
                    imgPause.setImageResource(R.drawable.ic_pause);
                    imgSheetPause.setImageResource(R.drawable.ic_pause);
                    Log.d("zzzzzzzzzzzz", "onReceive: " + seekProgress);
                } else {
                    isPause = false;
                    seekProgress = 0;
                    imgPause.setImageResource(R.drawable.ic_pause);
                    imgSheetPause.setImageResource(R.drawable.ic_pause);
                }
                Log.d("status", "onReceive: " + status);
                setTextBottomSheet(songPosition);
                count();
                listenSeekBar();
            }
        };

        getIntentFromService();
        IntentFilter filter = new IntentFilter("dataFromService");
        registerReceiver(receiver, filter);
        setTextBottomSheet(songPosition);
        listenBottomSheet();
    }

    private int getAllSecond() {
        return (Integer.parseInt(list.get(songPosition).getDuration()) / 1000);
    }

    public String getTime() {
        int minutes = seekBar.getProgress() / 60;
        int second = seekBar.getProgress() % 60;
        return minutes + ":" + second;
    }


    private void getIntentFromService() {
        is = getIntent().getStringExtra("is");
        if (is != null){
           songPosition = Integer.parseInt(getIntent().getStringExtra("position"));
            if (!is.equals("pause")) {
                isPause = false;
                imgPause.setImageResource(R.drawable.ic_pause);
                imgSheetPause.setImageResource(R.drawable.ic_pause);
            } else {
                isPause = true;
                imgPause.setImageResource(R.drawable.ic_play);
                imgSheetPause.setImageResource(R.drawable.ic_play);
            }
        }
    }
    private void listenSeekBar() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (seekProgress == getAllSecond()) {
                    seekBar.setProgress(0);
                }
                tvTime.setText(getTime());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekProgress = seekBar.getProgress();
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putInt("currentPosition", seekProgress);
                intent.putExtras(bundle);
                intent.putExtra("isGet", true);
                Log.d("aaaaaaaaa", "onReceive: " + seekProgress);
                intent.setAction("positionData");
                sendBroadcast(intent);
            }
        });
    }

    private void count() {

        handler.postDelayed(myRunnable, delay);
    }

    private void anhXa() {
        rcv = findViewById(R.id.rcv);
        cLayout = findViewById(R.id.c_layout);
        singerName = findViewById(R.id.tv_singer_name);
        songName = findViewById(R.id.tv_song_name);
        imgPause = findViewById(R.id.img_pause);
        imgStop = findViewById(R.id.imgStop);
        imgNext = findViewById(R.id.img_next);
        imgPrevious = findViewById(R.id.img_previous);
        ctlBottomSheet = findViewById(R.id.ctl_bottomsheet);
        imgDown = findViewById(R.id.img_down);
        tvSheetSingerName = findViewById(R.id.tv_sheet_singer_name);
        tvSheetSongName = findViewById(R.id.tv_sheet_song_name);
        imgSheetNext = findViewById(R.id.img_sheet_next);
        imgSheetPause = findViewById(R.id.img_sheet_pause);
        imgSheetPrevious = findViewById(R.id.img_sheet_previous);
        seekBar = findViewById(R.id.seekBar);
        tvTime = findViewById(R.id.tv_time);
    }

    private void listenBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(ctlBottomSheet);
        cLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });
        imgDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    private void clickNextOrPrevious() {
        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                songPosition++;
                if (songPosition == list.size()) songPosition = 0;
                NextOrPrevious();

            }
        });
        imgSheetNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                songPosition++;
                if (songPosition == list.size()) songPosition = 0;
                NextOrPrevious();
            }
        });
        imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                songPosition--;
                if (songPosition < 0) songPosition = list.size() - 1;
                NextOrPrevious();
            }
        });
        imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                songPosition--;
                if (songPosition < 0) songPosition = list.size() - 1;
                NextOrPrevious();
            }
        });
    }

    private void NextOrPrevious() {
        imgPause.setImageResource(R.drawable.ic_pause);
        imgSheetPause.setImageResource(R.drawable.ic_pause);
        seekProgress = 0;
        seekBar.setMax(getAllSecond());
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        is = "run";
        bundle.putString("is", is);
        bundle.putInt("position", songPosition);
        intent.putExtras(bundle);
        intent.setAction("positionData");
        sendBroadcast(intent);
        setTextBottomSheet(songPosition);
    }

    private void setTextBottomSheet(int position) {
        songName.setText(list.get(position).getTitle());
        tvSheetSongName.setText(list.get(position).getTitle());
        singerName.setText(list.get(position).getArtist());
        tvSheetSingerName.setText(list.get(position).getArtist());
    }

    public List<Song> getList() {
        list = new ArrayList<>();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

        String[] projection = {
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,

        };

        Cursor cursor = getBaseContext().getContentResolver().query(uri, projection, selection, null, null);
        int imageColumn = cursor.getColumnIndex(MediaStore.Audio.AlbumColumns.ALBUM_ART);
        while (cursor.moveToNext()) {
            list.add(new Song(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), imageColumn));
        }
        return list;
    }

    private void clickPause() {
        imgPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPause) {
                    isPause = false;
                    is = "continue";
                    isPause();
                    imgPause.setImageResource(R.drawable.ic_play);
                    imgSheetPause.setImageResource(R.drawable.ic_play);
                } else {
                    isPause = true;
                    is = "pause";
                    isPause();
                    imgPause.setImageResource(R.drawable.ic_pause);
                    imgSheetPause.setImageResource(R.drawable.ic_pause);
                }
            }
        });
        imgSheetPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPause) {
                    isPause = false;
                    is = "pause";
                    isPause();
                    imgSheetPause.setImageResource(R.drawable.ic_play);
                    imgPause.setImageResource(R.drawable.ic_play);
                } else {
                    isPause = true;
                    is = "continue";
                    isPause();
                    imgSheetPause.setImageResource(R.drawable.ic_pause);
                    imgPause.setImageResource(R.drawable.ic_pause);
                }
            }
        });
    }

    private void isPause() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("is", is);
        bundle.putInt("position", songPosition);
        intent.putExtras(bundle);
        intent.setAction("positionData");
        sendBroadcast(intent);
    }

    Intent serviceIntent;

    @Override
    public void sendData(int position) {
        songPosition = position;
        isPause = false;
        serviceIntent = new Intent(ListMusicActivity.this, MyService.class);
        seekBar.setProgress(0);
        startService();
        imgPause.setImageResource(R.drawable.ic_pause);
        imgSheetPause.setImageResource(R.drawable.ic_pause);
        setTextBottomSheet(position);
    }

    private void startService() {
        bundleService = new Bundle();
        isPause = false;
        bundleService.putSerializable("listSong", (Serializable) list);
        bundleService.putInt("position", songPosition);
        bundleService.putString("is", "run");
        serviceIntent.putExtras(bundleService);
        handler.removeCallbacks(myRunnable);
        seekProgress = 0;
        seekBar.setMax(getAllSecond());
        count();
        listenSeekBar();
        ContextCompat.startForegroundService(ListMusicActivity.this, serviceIntent);
    }

    public void stopService() {
        if (serviceIntent != null) {
            stopService(serviceIntent);
            serviceIntent = null;
            isPause = true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}

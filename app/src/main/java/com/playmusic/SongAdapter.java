package com.playmusic;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.List;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.SongHolder>{
    List<Song> list;
    Context context;
    DataFromAdapter dataFromAdapter;
    public SongAdapter(List<Song> list, Context context,  DataFromAdapter dataFromAdapter) {
        this.list = list;
        this.context = context;
        this.dataFromAdapter = dataFromAdapter;
    }

    @NonNull
    @Override
    public SongHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SongHolder holder;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_song, null, false);
        holder = new SongHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SongHolder holder, int i) {
        Song song = list.get(i);
        Gson gson = new Gson();
        holder.songName.setText(song.getTitle());
        holder.singerName.setText(song.getArtist());
        Log.d("song", gson.toJson(song));

        Glide.with(context)
                .load(song.getArt())
                .into(holder.logo);
        holder.ctl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService();
                dataFromAdapter.sendData(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    private void startService(){
        Intent intent = new Intent(context, MyService.class);
        ContextCompat.startForegroundService(context, intent);
    }
    class SongHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        ConstraintLayout ctl;
        TextView songName, singerName;
        public SongHolder(@NonNull View itemView) {
            super(itemView);
            songName = itemView.findViewById(R.id.tv_sheet_song_name);
            singerName = itemView.findViewById(R.id.tv_singer_name);
            logo = itemView.findViewById(R.id.img_logo);
            ctl = itemView.findViewById(R.id.ctl);
        }
    }


}


package com.playmusic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class ActionBroadcastReceiver extends BroadcastReceiver {
    public static final String NEXT = "Next";
    public static final String PREVIOUS = "Previous";
    public static final String PAUSE = "Pause";
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intent1 = new Intent();
        Bundle bundle = new Bundle();
        intent1.putExtra("FromNotification", true);
        String action = intent.getAction();
        if (NEXT.equals(action)){
            bundle.putInt("action", 2);
            Log.d("aaaaa", "next");
        }else if (PAUSE.equals(action)){
            Log.d("aaaaa", "pause");
            bundle.putInt("action", 1);
        }else if (PREVIOUS.equals(action)){
            bundle.putInt("action", 0);
            Log.d("aaaaa", "previous");
        }
        intent1.putExtras(bundle);
        intent1.setAction("positionData");
        context.sendBroadcast(intent1);
    }
}
